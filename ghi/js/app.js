function createCard(name, description, pictureUrl, starts, ends, subtitle) {
    return `
    <div class="col col-md-4">
      <div class="card shadow-none p-3 mb-5">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
            ${starts} - ${ends}
        </div>
      </div>
    </div>
    `;
  }

function error(status){
    return `
    <div class = "alert alert-primary" role = "alert">
    ${status}
    </div>
    `
}
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        const error = document.querySelector('.row');
        const error_status = response.status;
        error.innerHTML = error(error_status);
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const subtitle = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const start_date = new Date(starts);
            const end_date = new Date(ends);
            const start = start_date.toLocaleDateString()
            const end = end_date.toLocaleDateString()
            const html = createCard(
                title,
                description,
                pictureUrl,
                start,
                end,
                subtitle);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
